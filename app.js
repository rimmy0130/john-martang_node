const express = require('express');
const app = express();


const bodyParser = require('body-parser');
app.use(bodyParser.json());

const apiVersion1 = "v1";

// const fireBase = require("firebase-admin");
// app.use(fireBase);
const config = require('./config/config');

const fireBase = require("firebase-admin");
// const collectionname = "martang";

fireBase.initializeApp({
        credential: fireBase.credential.cert(config.firebasekey),
        databaseURL: "https://john-martang.firebaseio.com"
      });
// var martangDB = fireBase.firestore().collection(collectionname);


//home router
const homeRtr = require('./routes/home.router');
app.use(homeRtr);

// kofic router
const mapRtr = require('./routes/map.router');
app.use(`/api/${apiVersion1}`, mapRtr);


// 없는 주소
app.use(function(req, res) {
    res.status(404).send('앗! 페이지가 없어요.');
});



const PORT = process.env.PORT || 4000 ;
app.listen(PORT , ()=>{
    console.log(`server start || ${PORT}`);
})


