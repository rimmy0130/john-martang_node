
const request = require('request');
const {getFormatDate} = require('../utils/util');

/* FIREBASE */
const fireBase = require("firebase-admin");
const collectionname = "martang";
var martangDB = fireBase.firestore().collection(collectionname);

/* STORAGE */
const { Storage } = require('@google-cloud/storage');
const { format } = require('util');
const Multer = require('multer');
const multer = Multer();
const storage = new Storage({keyFilename: "./config/john-martang-325d82d1faba.json", 'projectId': 'john-martang'});
const bucket = storage.bucket("gs://john-martang.appspot.com/");  

/* KAKAO */
const kakaomapUrl = "https://dapi.kakao.com//v2/local/search/keyword.json"; //쪼개면 조타~  //TODO: config 파일로 쪼개보자,,,,
const kakaoKey = "KakaoAK 9b5dccb5a63412f98de56797eaf4d40c";

/* Category */
const fs = require('fs');
const path = require('path');


class kakaoService {

    searchKeyword(placeName, cateCode){
        return new Promise((resolve,reject)=>{
            //kakao API에 request 보내기
            var options = {
                url : kakaomapUrl,
                method : 'GET',
                headers : {
                    "Authorization" : kakaoKey
                },
                qs : {
                    "query" : placeName,
                    "category_group_code" : cateCode
                }
            }

            request(options, (mErr, mRes, mBody)=>{
                // if(mErr) return reject({status : 'error', data : '현재 서비스가 원활하지 읺습니다. 잠시후 다시 시도해주세요.'})
                // resolve({status : 'success', data : JSON.parse(mBody)})
                if(mErr) return reject(JSON.parse(mErr))
                console.log("mBody");
                console.log(mBody)
                resolve(JSON.parse(mBody))
            })

        })

    }


    getCategoryCode(){
        try {
            var data = fs.readFileSync( path.join('models','category_code.json'),'utf-8'); //string
            if(typeof data === 'string') data = JSON.parse(data);
            var dataList = [];
            Object.keys(data).forEach(item=>{
                dataList.push({
                    code : item,
                    codeTxt : data[item]
                })

            })

            dataList.sort((a,b)=>{
                if (a.codeTxt > b.codeTxt) {
                    return 1;
                  }
                  if (a.codeTxt < b.codeTxt) {
                    return -1;
                  }
                  // a must be equal to b
                  return 0;
            });

            dataList.splice(0,0, {code: "All", codeTxt : "전체"});
            return dataList; 
            
        } catch (error) {
            return error;
        }
    }


    //메인 select Code 
    getMainCode(){
        try {
            var data = fs.readFileSync( path.join('models','foodkind.json'),'utf-8'); //string
            if(typeof data === 'string') data = JSON.parse(data);
            var foodkind = [];
            Object.keys(data).forEach(item=>{
                foodkind.push({
                    foodkindKey : item,
                    foodkindName : data[item]
                })

            })

            var data2 = fs.readFileSync( path.join('models','region.json'),'utf-8'); //string
            if(typeof data2 === 'string') data2 = JSON.parse(data2);
            var region = [];
            Object.keys(data2).forEach(item=>{
                region.push({
                    regionKey : item,
                    regionName : data2[item]
                })

            })

            return {foodkind : foodkind,
                    region   : region    };

        } catch (error) {
            return error;
        }

    }




    //.orderBy('placeName')
    getSavedList(){
        return new Promise((resolve,reject)=>{
            martangDB.where('delFlag', '==', "" ).orderBy('date','desc')
            .get()
            .then(data => {
                // console.log(data);
                var dataList = [];
                data.forEach((doc)=>{
                    var item = Object.assign({docId : doc.id},doc.data())
                    item.ymdDate = item.date.substring(0,4)+'년 '+item.date.substring(4,6)+'월 '+item.date.substring(6,8)+'일 ';
                    // console.log(item);
                    dataList.push(item);
                })
                // console.log(dataList);
                resolve(dataList);
            })
            .catch(err => {
                reject(err);
            })

        })

    }



/* storage 저장 */
/**
 * Upload the image file to Google Storage
 * @param {File} file object that will be uploaded to Google Storage
 */
// const uploadImageToStorage = (file) => {
    uploadImageToStorage(file){
    return new Promise((resolve, reject) => {

      if (!file) {
        reject('No image file');
      }
  
  
      let newFileName =  getFormatDate(new Date()) + `${file.originalname}`;
      let fileUpload = bucket.file(newFileName);
  
      const blobStream =  fileUpload.createWriteStream({
      metadata: {
          contentType: file.mimetype
      }
      });
  
  
  
      blobStream.on('error', (error) => {
          reject('Something is wrong! Unable to upload at the moment.');
      });
  
  
      
  
      blobStream.on('finish', () => {
          // const url = format(`https://storage.googleapis.com/${bucket.name}/${fileUpload.name}`); 
          const url = format(`gs://john-martang.appspot.com/${fileUpload.name}`); 
          resolve(url);
      });
  
      blobStream.end(file.buffer);
  
    });
  }

}//kakaoService END

module.exports = kakaoService;