const router = require('express').Router();
const config = require('../config/config');

const KakaoService = require('../services/kakaoService');
const kakaoService = new KakaoService();

const ValidatorService = require('../utils/validator');
const validatorService = new ValidatorService;



const nodeCache = require('node-cache');
const myCache = new nodeCache();
// Cache의 key = 사용자가 보낸 값

/* FIREBASE */
const fireBase = require("firebase-admin");
const collectionname = "martang";
var martangDB = fireBase.firestore().collection(collectionname);
/* STORAGE */
const { Storage } = require('@google-cloud/storage');
const { format } = require('util');
const Multer = require('multer');
const multer = Multer();
const storage = new Storage({keyFilename: "./config/john-martang-325d82d1faba.json", 'projectId': 'john-martang'});
const bucket = storage.bucket("gs://john-martang.appspot.com/");  




//키워드로 장소 검색
router.post('/place', async (req,res)=>{
    //받은 데이터에 대한 validation check
    const {error} = validatorService.placeValidator(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    //여기까지 왔다는건 placName이 존재한다는 뜻이므로 req.body.placeName 이 가능해진다.
    //캐싱
    var cached = myCache.get(req.body.placeName+req.body.cateCode);
    if(!cached){
        //service를 이용해서 request 보내기
        //result는 service에서 받아온 데이터
        var result = await kakaoService.searchKeyword(req.body.placeName, req.body.cateCode);
        
        var data = result.documents.map(item => {
            return {
                placeId : item.id,
                map : {
                    x : item.x,
                    y : item.y
                },
                place_name : item.place_name,
                category_name : item.category_name,
                deailLink : item.place_url,
                road_addr : item.road_address_name,
                addr : item.address_name
                // metadata :item.meta 
            }
        })

        var kData = { 
            mapdata : data,
            metadata : result.meta
        }

        //화면에 출력  //화면에 보내는 데이터
        res.send({
            isCached : false ,
            data : kData
        });
        
        myCache.set(req.body.placeName+req.body.cateCode, kData);
    }else{
        res.send({
            isCached : true ,
            data : cached
        });
    }
    


})


//카테고리
router.get('/category/code',(req,res)=>{
    var categories = kakaoService.getCategoryCode();
    res.json(categories);
})

//메인에서 사용될 코드들
router.get('/main/code', (req,res)=>{
    var code = kakaoService.getMainCode();
    res.json(code);

})


router.post('/martang', (req,res)=>{ 
    const {error} = validatorService.createVaildator(req.body);
    if(error){
        var result = {
            "data" : {
                "status" : "Fail",
                "msg"    : "입력된 값 중에 올바르지 않은 값이 있습니다. 확인 후 다시 시도해주시기 바랍니다." 
            },
            "error" : error
        }

        return res.status(400).send( result);
    } 

    martangDB.add(req.body)
    .then(data=>{
        var result = {
            "status" : "Success",
            "msg"    : "저장되었습니다." 
        }
        res.send(result);
    })
    .catch(err=>{
        console.log(err);
        var result = {
            "status" : "Fail",
            "msg"    : "정상적으로 저장되지 않았습니다. 다시 한 번 시도해주세요." 
        }
        res.send(result);
    })
})



//DB READ
router.get('/martang', async(req,res)=>{
    try {
        var result = await kakaoService.getSavedList();
        res.send(result);
    } catch (error) {
        res.send(error);
    }
    
})

router.put('/martang', (req,res)=>{
    const {docId} = req.body;
    if(!docId) return res.status(400).send("문서번호가 없습니다. 문제가 지속되면 관리자에게 문의하시기 바랍니다.");

    const {error} = validatorService.editVaildator(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    martangDB.doc(req.body.docId).update(req.body)
    .then(data=>{

        var result = {
            "status" : "Success",
            "msg"    : "수정되었습니다." 
        }
        res.send(result);
    })
    .catch(err=>{
        console.log(err);
        var result = {
            "status" : "Fail",
            "msg"    : "정상적으로 수정되지 않았습니다. 다시 한 번 시도해주세요." 
        }
        res.send(result);
    })

})



router.delete('/martang', (req,res)=>{
    const {docId} = req.body;
    if(!docId) return res.status(400).send("문서번호가 없습니다. 문제가 지속되면 관리자에게 문의하시기 바랍니다.");
    
    // const {error} = validatorService.docIdVaildator(req.body);
    // if(error) return res.status(400).send(error.details[0].message);


    //!  나에게 돌아온 docId가 유효한 id인지 먼저 체크를 DB로 확인 
    //안하면 죽을 수 있음

    martangDB.doc(req.body.docId).update(req.body)
    .then(data=>{

        var result = {
            "status" : "Success",
            "msg"    : "삭제되었습니다." 
        }
        res.send(result);
    })
    .catch(err=>{
        console.log(err);
        var result = {
            "status" : "Fail",
            "msg"    : "정상적으로 삭제되지 않았습니다. 다시 한 번 시도해주세요." 
        }
        res.send(result);
    })

})






/**
 파일 업로드 시작
 */
router.post('/file', multer.array("jmtFiles", 3), async(req, res) => {  //file은 form의 name 속성   multer.array("file1" ,3 )   multer.single("file1")
    const files = req.files;

    if(!files){res.status(400).send({
    status: 'Error',
    data : "파일이 존재하지 않습니다."
    })}


    try {
        var promise = files.map( async (file)=>{
          var data = await kakaoService.uploadImageToStorage(file);
          return {
            "name" : file.originalname,
            "fileUrl" : data
          }
        })
    
        var allData = await Promise.all(promise);
    
        res.status(200).send({
              status: 'Success',
              data : allData
        });
      } catch (error) {
        console.log(error);
        res.status(500).send({
          status: 'Error',
          data : "현재 서비스가 원활하지 않습니다. 잠시후 다시 시도해 주십시오."
        });
      }
});




module.exports = router;