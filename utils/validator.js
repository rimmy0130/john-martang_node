const joi = require('@hapi/joi');

class validator {


    placeValidator(data){  

        const schema = joi.object({ // /[\{\}\[\]\/?.,;:|\)*~`!^\-\_+<>@\#$%&\\\=\(\'\"]/ any().invalid('[',']','{','}','<','>','(',')','/','\\','|','?','.',',',';',':','`','~','!','@','#','$','%','^','&','*','-','_','\'','\"','+','=')
            placeName : joi.string().min(2).required()
            ,cateCode : joi.string().alphanum().empty("").max(3).optional().uppercase()
        })

        return schema.validate(data);

    }


    createVaildator(data){
        const schema = joi.object({
            placeName : joi.string().min(2).required(),
            address : joi.string().empty(""),
            region : joi.string().empty(""),
            regionName : joi.string().empty(""),
            placeNickName : joi.string(),
            price : joi.string().max(6),
            foodkind : joi.string().empty(""),
            foodkindName : joi.string().empty(""),
            recoMan : joi.string().empty(""),
            rating : joi.number(),
            comment : joi.string().empty(""),
            date : joi.string().min(14).max(14).required(),
            xyData: joi.array().items(joi.string().empty("")),
            delFlag: joi.string().max(1).empty(""),
            fileArr : joi.array().items(joi.any())
        })
        return schema.validate(data);
    }

    editVaildator(data){
        const schema = joi.object({
            docId : joi.required(),
            placeName : joi.string().min(2).required(),
            address : joi.string().empty(""),
            region : joi.string().empty(""),
            regionName : joi.string().empty(""),
            placeNickName : joi.string(),
            price : joi.string().max(6),
            foodkind : joi.string().empty(""),
            foodkindName : joi.string().empty(""),
            recoMan : joi.string().empty(""),
            rating : joi.number(),
            comment : joi.string().empty(""),
            date : joi.string().min(14).max(14).required(),
            xyData: joi.array().items(joi.string()),
            delFlag: joi.string().max(1).empty(""),
            fileArr : joi.array().items(joi.any())

        })
        return schema.validate(data);
    }

    docIdVaildator(data){
        const schema = joi.object({
            docId : joi.string().empty().required()
        })
        return schema.validate(data);
    }


}

module.exports = validator;
