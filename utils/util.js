

function getFormatDate(date){
    var year = date.getFullYear();              
    var month = (1 + date.getMonth());          
    month = month >= 10 ? month : '0' + month;  
    var day = date.getDate();                   
    day = day >= 10 ? day : '0' + day;        
    var hour = date.getHours();  
    hour = hour >= 10 ? hour : '0' + hour;       
    var minute = date.getMinutes();  
    minute = minute >= 10 ? minute : '0' + minute;       
    var second = date.getSeconds();  
    second = second >= 10 ? second : '0' + second;
    var millisecond = date.getMilliseconds();
    millisecond = millisecond >= 10 ? millisecond : '0' + millisecond;

    return  year + '' + month + '' + day + '_' + hour + '' + minute + '' + second + "" + millisecond;       
}

module.exports.getFormatDate = getFormatDate;